<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN','api.mailgun.net/v3/nexges.com'),
        'secret' => env('MAILGUN_SECRET','key-e5a2cdf907a216c01d967eaa1338c856'),
        // 'endpoint' => env('MAILGUN_ENDPOINT', 'https://api.mailgun.net/v3/nexges.com'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],


    'facebook' => [
        'client_id' => env("FACEBOOK_APP_ID",'388972091662754'),
        'client_secret' => env('FACEBOOK_APP_SECRET','5866744f3bbc14d6c3c2381dca2e419a'),
        'redirect' => env("FACEBOOK_CALLBACK_URL","http://localhost:8000/api/callback"),
    ],

];
