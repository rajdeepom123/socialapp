<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersFriendConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_friend_connection', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('friend_user_id')->unsigned();
            $table->smallInteger('friend_status')->default(0)->comment('1 for Request Sent, 2 for Request Received, 3 for Request Accepted ');
            $table->timestamps();
            $table->softDeletes();
            /**
             * Key Mapping
             */
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('friend_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->index('friend_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_friend_connection');
    }
}
