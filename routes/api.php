<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::get('facebookLogin', 'Api\FacebookLoginController@facebookLogin');

    Route::get('searchfriend', 'Api\UserFriendConnectionController@searchfriend')->middleware('APIMiddleware');
    Route::post('sendConnectionRequest', 'Api\UserFriendConnectionController@sendConnectionRequest')->middleware('APIMiddleware');
    Route::post('acceptConnectionRequest', 'Api\UserFriendConnectionController@acceptConnectionRequest')->middleware('APIMiddleware');
    Route::get('getReceiveConnectionRequest', 'Api\UserFriendConnectionController@getReceiveConnectionRequest')->middleware('APIMiddleware');
    Route::get('getAllFriends', 'Api\UserFriendConnectionController@getAllFriends')->middleware('APIMiddleware');
