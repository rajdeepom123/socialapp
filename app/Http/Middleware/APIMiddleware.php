<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Helpers\Utility\ResponseFormatter;
use App\Http\Controllers\BaseController;
use App\Model\UserDevices;

class APIMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $accessToken = $request->header('accessToken');
        if(isset($accessToken) && !empty($accessToken)){
            $checkApi=UserDevices::where(["access_token"=>$accessToken])->first();
            if(!is_object($checkApi)){
                $result['statusCode']=401;
                $result['status']=0;
                $result['result']=[];
                $result['$message']="Unauthoirsed Access..";
                return response()->json($result);
            }else{
                // $request->user_id=$checkApi->user_id;
// echo json_encode($request);dd();
\Auth::loginUsingId($checkApi->user_id);
                return $next($request);
            }
        }else{
            $result['statusCode']=401;
            $result['status']=0;
            $result['result']=[];
            $result['$message']="Access Token Not provided on header..";
            return response()->json($result);
        }
    }
}
