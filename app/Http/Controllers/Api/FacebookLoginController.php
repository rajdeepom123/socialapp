<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Socialite;
use App\Http\Controllers\BaseController;
use App\Model\Users;


class FacebookLoginController extends BaseController
{

    public function __contruct(){
      
    }

    public function facebookLogin(Request $request) {
        if(!$request->callback){
            return Socialite::driver('facebook')->redirect();
        }
        $user = Users::createOrGetUser(Socialite::driver('facebook')->user());
        return response()->json($user);
    }

}
