<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Http\Requests\SearchFriendRequest;
use App\Http\Requests\SendFriendRequest;
use App\Model\UserFriendConnection;


class UserFriendConnectionController extends BaseController
{

    public function __contruct(){

    }

    public function searchfriend(SearchFriendRequest $request) {
        $userId = \Auth::user()->id;
        $user = UserFriendConnection::searchfriend($userId,$request);
        return response()->json($user);
    }

    public function sendConnectionRequest(SendFriendRequest $request) {
        $userId = \Auth::user()->id;
        $user = UserFriendConnection::sendConnectionRequest($userId,$request);
        return response()->json($user);
    }

    public function acceptConnectionRequest(SendFriendRequest $request) {
        $userId = \Auth::user()->id;
        $user = UserFriendConnection::acceptConnectionRequest($userId,$request);
        return response()->json($user);
    }

    public function getReceiveConnectionRequest(Request $request) {
        $userId = \Auth::user()->id;
        $user = UserFriendConnection::getReceiveConnectionRequest($userId);
        return response()->json($user);
    }

    public function getAllFriends(Request $request) {
        $userId = \Auth::user()->id;
        $user = UserFriendConnection::getAllFriends($userId);
        return response()->json($user);
    }
}
