<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserDevices extends Model
{
    protected $table = 'user_devices';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $hidden = [ 'updated_at'];
    protected $fillable = ['user_id', 'access_token','created_at'];
    public function user() {
        return $this->belongsTo('App\Models\Users');
    }

    public static function createAccessToken($userId){
        if (isset($userId)) {
            UserDevices::where('user_id', $userId)->delete();
        }
        $deviceToken = new UserDevices();
        $deviceToken->user_id = $userId;
        $deviceToken->access_token = md5($deviceToken->id . $userId . time());
        $deviceToken->save();
        return $deviceToken->access_token;
    }
}
