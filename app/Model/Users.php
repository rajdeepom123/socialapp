<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Helpers\Utility\ResponseFormatter;
use App\Model\UserDevices;
use App\Model\UserFriendConnection;
use Illuminate\Support\Facades\DB;

class Users extends Model
{
    protected $table = 'users';

    use SoftDeletes;
    protected $response;


    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $hidden = [ 'updated_at', 'deleted_at', 'password'];
    protected $fillable = ['name', 'email', 'password','provider_user_id','created_at'];

    public function _contruct(){
        $this->response = new ResponseFormatter();
    }
    public function device() {
        return $this->hasOne(UserDevices::class, 'user_id', 'id');
    }
    public function userConnectionsUser() {
        return $this->hasMany(UserFriendConnection::class, 'user_id', 'id');
    }
    public function userFriendsUserId() {
        return $this->hasMany(UserFriendConnection::class, 'friend_user_id', 'id');
    }

    public static function createOrGetUser($facebookUserData){
        $account = Users::where(['provider_user_id'=>$facebookUserData->getId()])
                    ->first();

        if ($account) {
            $account->accessToken=UserDevices::createAccessToken($account->id);


        } else {

            $account = new Users();
            $account->name=$facebookUserData->getName();
            $account->email=$facebookUserData->getEmail();
            $account->password=md5(rand(1,10000));
            $account->provider_user_id=$facebookUserData->getId();
            $account->save();
            $account->accessToken=UserDevices::createAccessToken($account->id);



        }

        $result['statusCode']=200;
        $result['status']=1;
        $result['result']=$account;
        $result['message']="Sucessfully Logged in..";
        return $result;
    }

    public static function getSearchFriend($userId,$data){
        return Users::select("users.name","users.email","users.id as user_id",DB::raw("case when user_friend_connection.friend_status=1 then 'Request Sent'
                    when user_friend_connection.friend_status=2 then 'Request Received'
                    when user_friend_connection.friend_status=3 then 'Request Accepted'
                    else 'Add Friend'
                    end as friend_status"))
                ->where(function($query) use ($data){
                    $query->orWhereRaw('LOWER(users.name) like "%' . $data["q"] . '%"')
                        ->orWhereRaw('LOWER(users.email) like  "%' . $data["q"] . '%"');
                })
                ->leftJoin("user_friend_connection","user_friend_connection.friend_user_id","users.id")
                ->where("users.id","!=",$userId)
                -> get();
    }

    public static function getUserData($userId){
        return Users::where("id",$userId)
                -> first();
    }
}
