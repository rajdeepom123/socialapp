<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserFriendConnection extends Model
{
    protected $table = 'user_friend_connection';


    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $hidden = [ 'updated_at'];
    protected $fillable = ['user_id', 'friend_user_id','friend_status','created_at'];

    public function usersUserId() {
        return $this->belongsTo(Users::class, 'user_id', 'id');
    }
    public function usersFriendUserId() {
        return $this->belongsTo(Users::class, 'friend_user_id', 'id');
    }

    public static function searchFriend($userId,$data){
        $userList =Users::getSearchFriend($userId,$data);
        $result['statusCode']=200;
        $result['status']=1;
        $result['result']=$userList;
        $result['message']="Data Not Found";
        if($userList->count()>0){
            $result['message']="Data Found";
        }
        return $result;
    }

    public static function sendConnectionRequest($userId,$data){
        $friendUser =Users::getUserData($data['friendUserId']);
        $result['statusCode']=200;
        $result['status']=1;
        $result['result']=[];
        $result['message']="Friend Request Sent Successfully...";
        if($friendUser){
            $whereClause=["user_id"=>$userId,"friend_user_id"=>$data['friendUserId']];
            $friendRequest=UserFriendConnection::where($whereClause)->first();
            if(!$friendRequest){
                $userConnection = new UserFriendConnection();
                $userConnection->user_id=$userId;
                $userConnection->friend_user_id=$data['friendUserId'];
                $userConnection->friend_status=1;
                $userConnection->save();

                $userFriendConnection = new UserFriendConnection();
                $userFriendConnection->user_id=$data['friendUserId'];
                $userFriendConnection->friend_user_id=$userId;
                $userFriendConnection->friend_status=2;
                $userFriendConnection->save();

                $user=Users::getUserData($userId);

                \Mail::send('emails.send-friend-request', ['name' => ucwords($friendUser->name),'friendUserName' => ucwords($user->name), 'email' => $friendUser->email, 'org_name' => config('constant.ORG_NAME')], function($message) use ($friendUser) {
                    $message->to($friendUser->email, ucwords($friendUser->name))->subject('Friend Request');
                    $message->from(config('constant.ADMIN_EMAIL'), config('constant.ORG_NAME'));
                });

            }else{
                $result['statusCode']=200;
                $result['status']=0;
                $result['result']=[];
                $result['message']="Friend Request Already Sent...";
            }
        }else{
            $result['statusCode']=200;
            $result['status']=0;
            $result['result']=[];
            $result['message']="Friend Does not exists...";
        }
        return $result;
    }


    public static function acceptConnectionRequest($userId,$data){
        $friendUser =Users::getUserData($data['friendUserId']);
        $result['statusCode']=200;
        $result['status']=1;
        $result['result']=[];
        $result['message']="Friend Request Accepted Successfully...";
        if($friendUser){
            $whereClause=["user_id"=>$userId,"friend_user_id"=>$data['friendUserId'], "friend_status"=>config('constant.FRIEND_STATUS.REQUEST_RECEIVED')];
            $friendRequest=UserFriendConnection::where($whereClause)->first();
            if($friendRequest){
                 UserFriendConnection::where(function ($query) use ($userId, $data) {

                    $query->where('user_id', $userId)
                        ->where('friend_user_id',  $data['friendUserId']);
                })->orWhere(function($query) use ($userId, $data) {
                    $query->where('friend_user_id', $userId)
                    ->where('user_id',  $data['friendUserId']);
                })
                ->update(["friend_status"=>config('constant.FRIEND_STATUS.REQUEST_ACCEPTED')]);

            }else{
                $result['statusCode']=200;
                $result['status']=0;
                $result['result']=[];
                $result['message']="Friend Request Has Not Sent...";
            }
        }else{
            $result['statusCode']=200;
            $result['status']=0;
            $result['result']=[];
            $result['message']="Friend Does not exists...";
        }
        return $result;
    }



    public static function getReceiveConnectionRequest($userId){
        $result['statusCode']=200;
        $result['status']=1;
        $result['result']=[];
        $result['message']="Data Found...";

            $whereClause=["user_id"=>$userId,"friend_status"=>config('constant.FRIEND_STATUS.REQUEST_RECEIVED')];
            $friendRequest=UserFriendConnection::select("*",DB::raw("case when friend_status=1 then 'Request Sent'
            when friend_status=2 then 'Request Received'
            when friend_status=1 then 'Request Accepted'
            else 'Add Friend'
            end as friend_status"))->where($whereClause)->with("usersFriendUserId")->get();
            if($friendRequest){
                $result['statusCode']=200;
                $result['status']=1;
                $result['result']=$friendRequest;
                $result['message']="Data  Found...";

            }else{
                $result['statusCode']=200;
                $result['status']=0;
                $result['result']=[];
                $result['message']="Data Not Found...";
            }

        return $result;
    }

    public static function getAllFriends($userId){
        $result['statusCode']=200;
        $result['status']=1;
        $result['result']=[];
        $result['message']="Data Found...";

            $whereClause=["user_id"=>$userId,"friend_status"=>config('constant.FRIEND_STATUS.REQUEST_ACCEPTED')];
            $friendRequest=UserFriendConnection::select("*",DB::raw("case when friend_status=1 then 'Request Sent'
            when friend_status=2 then 'Request Received'
            when friend_status=3 then 'Request Accepted'
            else 'Add Friend'
            end as friend_status"))->where($whereClause)->with("usersFriendUserId")->get();
            if($friendRequest){
                $result['statusCode']=200;
                $result['status']=1;
                $result['result']=$friendRequest;
                $result['message']="Data Found...";

            }else{
                $result['statusCode']=200;
                $result['status']=0;
                $result['result']=[];
                $result['message']="Data Not Found...";
            }

        return $result;
    }

}
